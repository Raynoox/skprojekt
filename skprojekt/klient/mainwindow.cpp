#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //ustawienie startowego wygladu okienka
    ui->setupUi(this);
    ui->textEditLogin->setVisible(false);
    ui->textEditPassword->setVisible(false);
    ui->labelLogin->setVisible(false);
    ui->labelPassword->setVisible(false);
    ui->registrationButton->setVisible(false);
    ui->loginButton->setVisible(false);
    ui->addMsgButton->setVisible(false);
    ui->showMsg->setVisible(false);
    ui->showButton->setVisible(false);
    ui->subscribeButton->setVisible(false);
    ui->sendButton->setVisible(false);
    ui->plainTopicEdit->setVisible(false);
    ui->plainTextNewMsg->setVisible(false);
    this->port = 2000;
    //button powrotu z postow
    ui->backButton->setVisible(false);

    //view dla postow, poczatkowo ukryty
    ui->postsView->setVisible(false);
    ui->topicView->setVisible(false);
    ui->addressTextEdit->setText("127.0.0.1");
}

//wyswietlenie zawartosci tematów
void MainWindow::goToTopic(){
    whichButton="topic";
    ui->plainTopicEdit->setVisible(true);
    QModelIndex index = ui->topicView->currentIndex();
    ui->labelTopic->setText(index.data().toString());

    ui->backButton->setVisible(true);
    ui->topicView->setVisible(false);
	
    //wyslanie wiadomosci proszacej o posty z tematu
    QString text = "showTopic\n";
    text.append(index.data().toString()+"\n");
    QByteArray qb1 = text.toLatin1();
    char* toSend = qb1.data();
    socket->write(toSend, strlen(toSend));
    ui->plainTextEdit->appendPlainText("\n wyslano prosbe o posty z "+index.data().toString());


}
//wyslanie prosby o swoje subskrypcje
void MainWindow::goToSub()
{
    whichButton="sendSub\n";
    QString text = "sendSub\n";
    QModelIndex index = ui->topicView->currentIndex();
    ui->labelTopic->setText(index.data().toString());
    text.append(index.data().toString()+"\n");
    QByteArray qb1 = text.toLatin1();
    char* toSend = qb1.data();
    socket->write(toSend, strlen(toSend));
    ui->topicView->setVisible(false);
}

//przycisk powrotu z tematow
void MainWindow::on_backButton_clicked(){
    ui->addMsgButton->setVisible(false);
    ui->backButton->setVisible(false);
    ui->plainTopicEdit->setVisible(false);
    ui->topicView->setVisible(true);
    ui->sendButton->setVisible(false);
}
//destruktor okna
MainWindow::~MainWindow()
{
    delete socket;
    delete ui;
}

void MainWindow::on_pushButton_clicked(){
    QString host = ui->addressTextEdit->toPlainText();
    ui->plainTextEdit->appendPlainText("Looking up host" + host + "...");
    QHostInfo::lookupHost(host,this, SLOT(hostLookedUp(const QHostInfo&)));
}

//obsluga przycisku rejestracji
void MainWindow::on_registrationButton_clicked(){
    ui->textEditLogin->setVisible(true);
    ui->textEditPassword->setVisible(true);
    ui->labelLogin->setVisible(true);
    ui->labelPassword->setVisible(true);
    ui->sendButton->setVisible(true);
      whichButton="registration";
}
//obsluga przycisku logowania
void MainWindow::on_loginButton_clicked(){
    ui->textEditLogin->setVisible(true);
    ui->textEditPassword->setVisible(true);
    ui->labelLogin->setVisible(true);
    ui->labelPassword->setVisible(true);
    ui->sendButton->setVisible(true);
    whichButton="login";
}

//obsluga przycisku wyswietlajacego tematy
void MainWindow::on_showButton_clicked(){
      QString text = "showall\n";
      whichButton="showTopic";
      QByteArray qb1 = text.toLatin1();
      char* toSend = qb1.data();
      socket->write(toSend, strlen(toSend));
}

//obsluga przycisku subskrypcji
void MainWindow::on_subscribeButton_clicked(){
      whichButton="subscribe";
      QString text = "showall\n";
      QByteArray qb1 = text.toLatin1();
      char* toSend = qb1.data();
      socket->write(toSend, strlen(toSend));
}

//obsluga przycisku dodajacego post
void MainWindow::on_addMsgButton_clicked(){
      ui->sendButton->setVisible(true);
      ui->plainTextNewMsg->setVisible(true);
}

//obsluga zdarzenia odnalezienia hosta
void MainWindow::hostLookedUp(const QHostInfo &info){
    if(info.error() != QHostInfo::NoError){
           ui->plainTextEdit->appendPlainText(info.errorString());
           ui->addressTextEdit->setVisible(false);
           return;
       }

    foreach (QHostAddress address, info.addresses())
        ui->plainTextEdit->appendPlainText(QString("Found address: ") + address.toString());

    this->hostInfo = info;

    this->socket = new QTcpSocket(this);

    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this,
                    SLOT(socketError(QAbstractSocket::SocketError)));
    connect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readAnswer()));

    socket->connectToHost(info.hostName(), (quint16) port);
}

//obsluga bledow socketa
void MainWindow::socketError(QAbstractSocket::SocketError err){
    ui->plainTextEdit->appendPlainText("Connection error" + err);
}

//obsluga zdarzenia polaczenia socketa z hostem
void MainWindow::socketConnected(){
    ui->plainTextEdit->appendPlainText("Connected");


            QString req = "conn\n";

            QByteArray qb = req.toLatin1();
            char* encReq = qb.data();
            socket->write(encReq, strlen(encReq));
            ui->plainTextEdit->appendPlainText("Sent...");
            ui->pushButton->setVisible(false);
            ui->registrationButton->setVisible(true);
            ui->loginButton->setVisible(true);

}

//obsluga przycisku wysylania komunikatow
void MainWindow::on_sendButton_clicked(){
      QString text = "test";
      ui->statusBar->showMessage(whichButton,20000);
      if(whichButton=="registration" || whichButton=="login")
      {
          text= "";
          text.append(whichButton+"\n");
          if(ui->textEditPassword->toPlainText()!="" && ui->textEditLogin->toPlainText() != ""){
          text.append(ui->textEditLogin->toPlainText()+"\n");
          text.append(ui->textEditPassword->toPlainText()+"\n");
          }
          else
          {
          ui->statusBar->showMessage("Wpisz poprawny login i haslo",20000);
          return;
          }
      }
      if(whichButton=="topic")
      {
          text ="";
          if(ui->plainTextNewMsg->toPlainText()!=""){
          text.append(whichButton+"\n");
          QModelIndex index = ui->topicView->currentIndex();
          ui->labelTopic->setText(index.data().toString());
          text.append(index.data().toString()+"\n"); //topic number zmieniac przy wyborze któregoś tematu
          //text.append("Finanse\n");
          text.append(ui->plainTextNewMsg->toPlainText()+"\n");
          ui->plainTextNewMsg->setVisible(false);
          ui->plainTextNewMsg->clear();
          }
          else
          {
              ui->statusBar->showMessage("ERROR: Musisz wpisac cos do wiadomosci",20000);
          }
      }
      QByteArray qb1 = text.toLatin1();
      char* toSend = qb1.data();
      socket->write(toSend, strlen(toSend));
      ui->plainTextEdit->appendPlainText("Sent "+whichButton+" message");
}

//odczytywanie wiadomosci z serwera
void MainWindow::readAnswer(){
    int rb =0;
    char buf[1024];
    ui->plainTextEdit->appendPlainText("Got answer");
    if(whichButton=="login")
    {
        while((rb= socket->read(buf, 1024))>0){
        ui->plainTextEdit->appendPlainText(QString::fromLatin1(buf,rb));

        if(strcmp(buf,"zostales zalogowany\n")==0)
        {
            ui->showButton->setVisible(true);
            ui->subscribeButton->setVisible(true);
            ui->sendButton->setVisible(true);

            ui->textEditLogin->setVisible(false);
            ui->textEditPassword->setVisible(false);
            ui->labelLogin->setVisible(false);
            ui->labelPassword->setVisible(false);
            ui->loginButton->setVisible(false);
            ui->registrationButton->setVisible(false);
            ui->plainTextEdit->appendPlainText(QString::fromLatin1(buf,rb));
        }
        }

    }
    while ((rb= socket->read(buf, 1024))>0)
       { ui->plainTextEdit->appendPlainText(QString::fromLatin1(buf,rb));
        ui->plainTextEdit->appendPlainText(QString::number(rb));
    if(whichButton=="showTopic")
    {
        on_backButton_clicked();

        list.clear();
        int i=0;
        QString temp="";
        while(buf[i]!='\n')
        {
        while(buf[i]!=';')
            temp.append(buf[i++]);
        i++;
        ui->plainTextEdit->appendPlainText("temat- "+temp);
        list<<temp;
        temp="";
        }

        QStringListModel *model = new QStringListModel(this);
        model->setStringList(list);
        ui->topicView->setModel(model);
        ui->topicView->setSelectionMode(QAbstractItemView::SingleSelection);
        connect(ui->topicView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)), this, SLOT(goToTopic()));
        whichButton="tempNull";
        on_backButton_clicked();
         }
    if(whichButton=="subscribe")
    {
        list.clear();
        list << "Polityka" << "Finanse" << "Gry" << "Sport" << "Rozrywka" << "Random";
        int i=0;
        QString temp="";
        while(buf[i]!='\n')
        {
        while(buf[i]!=';')
            temp.append(buf[i++]);
        i++;
        for(int p=0;p<6;p++)
        {
        ui->plainTextEdit->appendPlainText("\n"+list[p]);
            if(list[p]==temp)
            {
                list.removeAt(p);
                p=7;
            }
        }
        temp="";
        }


        QStringListModel *model = new QStringListModel(this);
        model->setStringList(list);
        ui->topicView->setModel(model);
        ui->topicView->setSelectionMode(QAbstractItemView::SingleSelection);
        connect(ui->topicView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)), this, SLOT(goToSub()));
        whichButton="tempNull";
        on_backButton_clicked();
    }
        if(whichButton=="topic")
        {
            QString temp="";
            int x = 0;
            while(x<rb)
            {
                temp.append(buf[x++]);
            }
            ui->plainTopicEdit->setPlainText(temp);
            ui->addMsgButton->setVisible(true);
        }

    }
}


