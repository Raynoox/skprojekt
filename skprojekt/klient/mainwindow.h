#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QTcpSocket>
#include<QString>
#include<QHostInfo>
#include<QStringListModel>
#include<QStringList>
#include<QItemSelectionModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QTcpSocket *socket;
    qint64 port;
    QHostInfo hostInfo;
    QString whichButton = "";
    QStringList list;
    QString chosenTopic;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void on_pushButton_clicked();
    void on_sendButton_clicked();
    void on_registrationButton_clicked();
    void on_loginButton_clicked();
    void on_showButton_clicked();
    void on_subscribeButton_clicked();
    void on_addMsgButton_clicked();
    void hostLookedUp(const QHostInfo& info);
    void socketError(QAbstractSocket::SocketError);
    void socketConnected();
    void readAnswer();
    void goToTopic();
    void goToSub();
    void on_backButton_clicked();


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
