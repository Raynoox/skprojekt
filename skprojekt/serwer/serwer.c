#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#define CON_LIMIT 128
#define PORT 2000
#define BUFSIZE 1024;

typedef int bool;
#define true 1
#define false 0

pthread_t client_threads [CON_LIMIT];
pthread_t main_thread;
pthread_mutex_t sock_mutex = PTHREAD_MUTEX_INITIALIZER;
int cln_sockets [CON_LIMIT];

//wyslanie wiadomosci do klienta
void sendMsgToUser(int sck, char* msg)
{
send (sck, msg, strlen(msg), 0);
}

//funkcja dodajaca subskrypcje uzytkownikowi
void addSub(int i,char * buffer,char * nick, int sck)
{
FILE *fp;
fp = fopen("subUser.txt", "r");
char line_ptr[1024];
char tempNick[1024]="";
char tempFile[1024] = "";
char subTopic[1024]="";
int j = 0;
//wczytanie topicu z wiadomosci ( do ktorego topicu user chce zasubskrybowac
while(buffer[i] != '\n'){
		subTopic[j++] = buffer[i++];
}

	
	memset(&line_ptr[0], 0, sizeof(line_ptr));
	int foundSub = 0;
	//sprawdzenie subUser.txt linia po linii
	while(fgets(line_ptr, sizeof(line_ptr), fp))
	{
		strcat(tempFile,line_ptr);
		strncpy(tempFile,tempFile, strlen(tempFile)-1);
		printf("%s",line_ptr);
		int i = 0;
		j = 0;
		memset(&tempNick[0], 0, sizeof(tempNick));
		//wczytanie nicku z linii
		while(line_ptr[i]!=';'){
		
		tempNick[i]=line_ptr[i];
		i++;
		}
		
		i++;
		//sprawdzenie czy nick uzytkownika zgadza sie ze znalezionym nickiem w pliku
		if(strcmp(tempNick, nick) == 0 && foundSub == 0)
		{
			//dopisanie subskrypcji na koniec linii
			foundSub = 1;
			strcat(subTopic,";\n");
			tempFile[strlen(tempFile)-1]=0;
			strcat(tempFile,subTopic);
		}
		
		
		memset(&line_ptr[0], 0, sizeof(line_ptr));
	}

fclose(fp);
fp = fopen("subUser.txt","w");
if( fp!=NULL)
{
fputs(tempFile, fp);
fclose(fp);
}
printf("\nsubskrypcja zakonczon \n");
}



//funkcja przeprowadzajaca rejestracje uzytkownika
void rejestracja(int i, char* buffer,int sck){
	
	//zmienne do kopiowania danych o uzytkowniku
	char nick[1024] = "";
	char password[1024] = "";
	int j = 0;
	char userInfo[1024] = ""; //bufor trzymajacy pelne dane o uzytkowniku-login i haslo
	FILE *fp;
	
	//przekopiowanie loginu
	while(buffer[i] != '\n'){
		nick[j++] = buffer[i++];
	}

	i++;
	j=0;
	
	//przekopiowanie hasla
	while(buffer[i] != '\n'){
		password[j++] = buffer[i++];
	}

	//skopiowanie loginu i hasla wg schematu trzymania danych w pliku
	strcat(userInfo, nick);
	strcat(userInfo, ";");
	strcat(userInfo, password);

	
	fp = fopen("users.txt", "r");
	char line_ptr[1024];
	char tempnick[1024];
	printf("getdelim\n");
	while(fgets(line_ptr, sizeof(line_ptr), fp))
	{
		//sprawdzenie, czy istnieje juz uzytkownik o wybranym loginie
		memset(&tempnick[0], 0, sizeof(tempnick));
		printf("%s\n",line_ptr);
		int i =0;
		while(line_ptr[i] != ';')
		{
			tempnick[i] = line_ptr[i];
			i++;	
		}
		printf("%s",tempnick);
		if(strcmp(tempnick, nick) == 0)
		{
			fclose(fp);
			printf("Podany login jest juz uzywany\n");
			sendMsgToUser(sck,"Podany login jest juz uzywany\n");
			return;	
		}
	
	}
	fclose(fp);
	
	//zapisanie nowego uzytkownika do pliku
	if((fp=fopen("users.txt", "a")) != NULL)
	{
		fprintf(fp, "%s\n", userInfo);
		FILE* fp2;
		//dodanie uzytkownika do pliku z subskrypcjami
		fp2 = fopen("subUser.txt","a");
		if(fp2 != NULL)
		{
			
			printf("dodany nick i haslo - %s", nick);
			strcat(nick,";\n");
			fputs(nick,fp2);
		}
		fclose(fp2);
		
		sendMsgToUser(sck,"Zostales zarejestrowany\n");
	}
	else
	{
		printf("Cannot open file with user data\n");
		return;
	}
	fclose(fp);
}


void logowanie(int i,char* buffer,int sck,char ** logged)
{
	//zmienne do skopiowania loginu i hasla
	char nick[1024] = "";
	char password[1024] = "";
	char nick2[1024]= "";
	int j = 0;

	//kopiowanie loginu
	while(buffer[i] != '\n'){
		nick[j++] = buffer[i++];
	}

	i++;
	j=0;
	//kopiowanie hasla
	while(buffer[i] != '\n'){
		password[j++] = buffer[i++];
	}

	printf("Logowanie - nick: %s \t password: %s\n",nick,password);

	strcat(nick2,nick);

	char userInfo[1024] = "";

	strcat(userInfo, nick);
	strcat(userInfo, ";");
	strcat(userInfo, password);
	strcat(userInfo, "\n");

	FILE *fp;
	fp = fopen("users.txt", "r");
	char line_ptr[1024];
	bool zalogowany = false;

	//wyszukanie loginu w pliku z zarejestrowanymi uzytkownikami
	while(fgets(line_ptr, sizeof(line_ptr), fp) && !zalogowany)
	{
		printf("%s",line_ptr);
		if(strcmp(line_ptr, userInfo) == 0)
		{
			zalogowany = true;
			printf("Udalo znalezc sie pasujacy login i haslo\n");
			//sendMsgToUser(sck,"Zostales zarejestrowany");
		}
		memset(&line_ptr[0], 0, sizeof(line_ptr));
	}
	fclose(fp);
	
	//wyslanie odpowiedzi do klienta
	if(zalogowany)
	{
	sendMsgToUser(sck,"zostales zalogowany\n"); 
		*logged = malloc(strlen(nick2)+1);
		strcpy(*logged,nick2);
	}
	else
	{
		sendMsgToUser(sck,"bledne login lub haslo\n"); 
		logged = 0;
	}	
}


//dodanie posta przez klienta
void addMsg(int i, char* buffer, int sck)
{
	int j = 0;
	char topic[1024] = "";
	
	//sprawdzenie, do jakiego tematu dodac post
	while(buffer[i]!='\n')
		topic[j++] = buffer[i++];

	strcat(topic, ".txt");

	FILE *fp;
	char msg[1024] = "%NEWMSG%\n";

	j=9;
	i++;
	
	//skopiowanie tresci posta
	while(buffer[i])
		msg[j++]=buffer[i++];
	
	//umieszczenie posta w pliku i odeslanie go do klienta
	if((fp=fopen(topic, "a")) != NULL)
	{
		fprintf(fp, "%s", msg);
		sendMsgToUser(sck,msg);
	}
	else
	{
		printf("Cannot open file with user data\n");
		return;
	}
	fclose(fp);
}

//wyswietlenie wszystkich subskrybowanych przez klienta tematow
void showAll(char *nick, int sck)
{

	FILE *fp;
	fp = fopen("subUser.txt", "r");
	char line_ptr[1024];
	char tempNick[1024]="";
	char msgTopics[1024] = "";

	
	memset(&line_ptr[0], 0, sizeof(line_ptr));
	while(fgets(line_ptr, sizeof(line_ptr), fp))
	{

		int i = 0;
		int j = 0;
		//wyszukanie loginu uzytkwnika w pliku z subskrypcjami
		memset(&tempNick[0], 0, sizeof(tempNick));
		while(line_ptr[i]!=';'){
		
		tempNick[i]=line_ptr[i];
		i++;
		}

		i++;

		if(strcmp(tempNick, nick) == 0)
		{
			
			//zebranie listy subskrybowanych tematow i wyslanie ich do klienta
			while(line_ptr[i]!='\n')
			{
				if(line_ptr[i]!=';')
				msgTopics[j++] = line_ptr[i++];
		
				else
				{
					i++;
					msgTopics[j++]=';';
				}
			}
			msgTopics[j]= '\n';
			sendMsgToUser(sck,msgTopics);
		}
		memset(&line_ptr[0], 0, sizeof(line_ptr));
	}

	fclose(fp);

}

//wyslanie wszystkich postow z tematu do klienta
void showTopic(int i, char* buffer,int sck)
{
	int j = 0;
	char topic[1024] = "";
	char line_ptr[1024];

	//sprawdzenie, jaki temat nalezy wyslac
	while(buffer[i]!='\n')
		topic[j++] = buffer[i++];

	strcat(topic, ".txt");
	char msg[1024]="";
	strcat(msg,topic);
	strcat(msg,"\n");

	FILE *fp;
	j=0;
	i++;
	
	//zbieranie postow w temacie i wyslanie ich do klienta
	fp=fopen(topic,  "r");
	while(fgets(line_ptr, sizeof(line_ptr), fp))
	{
		if(strcmp(line_ptr,"%NEWMSG%\n")==0)
		{
		strcat(msg,"\nNASTEPNA WIADOMOSC \n");
	}
		else
			strcat(msg,line_ptr);

		memset(&line_ptr[0], 0, sizeof(line_ptr));
	}

	sendMsgToUser(sck,msg);
}

//petla obslugujaca polaczenie z klientem
void* client_loop (void* arg) 
{
	char buffer [1024];
	char command [1024];
	int sck = *((int*) arg);
	int i;
	int rcvd;
	char * nick = "";
	char msg[] = "Odpowiedz serwera";
	printf ("New client thread started for socket %d\n", sck);
	if ((rcvd = recv(sck,buffer,1024,0)) > 0) ;

	printf("%s",buffer);
	printf("conn complete\n");

	i = 0;
	
	printf("1\n");
	while((rcvd = recv(sck, buffer,1024,0)) > 0)	
	{
		printf("%s",buffer);
		while(buffer[i] != '\n'){
			printf("%c\n",buffer[i]);
			command[i] = buffer[i];
			i++;	
	}
	i++;
	printf("komenda - %s\n",command);
	//sprawdzenie typu przychodzacej wiadomosci
	//obsluga rejestracji
		if(strcmp(command, "registration")==0)
		{
			rejestracja(i,buffer,sck);
			i = 0;
		}
		
		//obsluga logowania
		if(strcmp(command, "login") == 0)
		{
			logowanie(i,buffer,sck,&nick);
			printf("zalogowalem nick hehe %s\n", nick);
			i=0;
		}

		//obsluga zadania o wyswietlenie tematow
		if(strcmp(command, "showall") == 0)
		{
			showAll(nick,sck);
			i=0;
		}

		//oblsuga dodawania postu do tematu
		if(strcmp(command, "topic") == 0)
		{
			addMsg(i,buffer,sck);
		}

		//obsluga wyswietlenia postow w temacie
		if(strcmp(command, "showTopic") == 0)
		{
			showTopic(i,buffer,sck);
		}	
		//obsluga dodawania subskrypcji
		if(strcmp(command, "sendSub")==0)
		{
			addSub(i,buffer, nick,sck);
		}			
		i=0;
		memset(&buffer[0], 0, sizeof(buffer));
		memset(&command[0], 0, sizeof(command));
	}	
	
	printf("pass");
	

	while ((rcvd = recv (sck, buffer, 1024, 0)) > 0){
		printf("Message on port: %d\n", sck);
		send (sck, msg, sizeof(msg), 0);
	}
	//zakonczenie polaczenia z klientem
	close (sck);
	pthread_mutex_lock (&sock_mutex);
	for (i = 0; i < CON_LIMIT; i++)
		if (cln_sockets [i] == sck) {
			cln_sockets [i] = 0;
			break;
		} 
	pthread_mutex_unlock (&sock_mutex); 

	printf ("Client thread ending for socket %d\n", sck);
	pthread_exit (NULL);
}

//glowna petla serwera przyjmujaca nowe polaczenia i tworzaca nowe porty dla klientow
void* main_loop (void* arg)
{
	int srv_socket, tmp_socket;
	socklen_t cln_addr_size;
	struct sockaddr_in srv_addr, cln_addr;

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons (PORT);

	bzero (cln_sockets, CON_LIMIT * sizeof (int));
	//stworzenie glownego socketa na porcie 2000
	if ((srv_socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		perror ("Main socket creation error:");
		exit (EXIT_FAILURE);
	}
	int enable = 1;
	if (setsockopt(srv_socket,SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int))<0)
		perror("setsockopt(SO_REUSEADDR) failed");
	if (bind (srv_socket, (struct sockaddr*) &srv_addr, sizeof srv_addr) == -1) {
		perror ("Main socket bind error:");
		exit (EXIT_FAILURE);
	}

	if (listen (srv_socket, 16) == -1) {
		perror ("Main socket listen error:");
		exit (EXIT_FAILURE);
	} 

	printf ("Main socket started\n");

	//tworzenie nowych portow dla klienta
	while (1) {
		cln_addr_size = sizeof cln_addr;
		if ((tmp_socket = accept (srv_socket, (struct sockaddr*) &cln_addr, &cln_addr_size)) == -1) {
			perror ("Main socket accept error:");
			continue;
		}

		pthread_mutex_lock (&sock_mutex);
		int i;
		for (i = 0; i < CON_LIMIT; i++)
			if (cln_sockets [i] == 0) {
				send(2000, (char*)tmp_socket, 10, 0);
				printf("Sent new port number\n");
				cln_sockets [i] = tmp_socket;
				break;
			} 
		pthread_mutex_unlock (&sock_mutex);

		if (i == CON_LIMIT) {
			printf ("Too many connections\n");
			close (tmp_socket);
			continue;
		}

		if (pthread_create (&client_threads [i], NULL, client_loop, &cln_sockets[i]) != 0) {
			printf ("Error creating new client thread\n");
			continue;
		} 
	}
}

int main (int argc, char** argv)
{
	if (pthread_create (&main_thread, NULL, main_loop, NULL) != 0) {
		printf ("Thread create error\n");
		exit (EXIT_FAILURE);
	}

	printf ("Main thread started\n");
	printf ("Press <ENTER> to shutdown server\n");
	getc (stdin);

	return EXIT_SUCCESS;
}

